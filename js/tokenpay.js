(function($, Drupal, drupalSettings) {
  'use strict';

  var tp = TokenPay(drupalSettings.bridgepayTokenPay.publicKey);
  Drupal.behaviors.bridgepayTokenPay = {
    attach: function(context) {
      $('input.token-target').once('token-pay').each(function() {
        let opts = {
          dataElement: $(this).siblings('.token-card').attr('id'),
          errorElement: $(this).siblings('.token-error-message').attr('id'),
          amountElement: null,
          useACH: false
        };
        tp.initialize(opts);

        $(this).closest('form').submit(function(e) {
          event.preventDefault();
          let form = this;
          tp.createToken(function (result) {
            $(form).find('input.token-target').val(result.token);
            $(form).unbind('submit').submit();
          }, function(result) {});
        })
      });

    }
  }

})(jQuery, Drupal, drupalSettings);

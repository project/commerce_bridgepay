<?php

namespace Drupal\commerce_bridgepay\Plugin\Commerce\PaymentGateway;

use BridgeComm\Client;
use BridgeComm\Exception\ResponseException;
use BridgeComm\Request;
use BridgeComm\RequestCredentials;
use BridgeComm\RequestCredentialsInterface;
use BridgeComm\RequestDefaultsInterface;
use BridgeComm\RequestFactory;
use BridgeComm\RequestMessage\ProcessPaymentRequestMessage;
use Drupal\commerce_bridgepay\BridgePayHttpClientFactory;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Calculator;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Payment gateway for integrating with bridgepay.
 *
 * @CommercePaymentGateway(
 *   id = "bridgepay",
 *   label = "BridgePay",
 *   display_label = "BridgePay",
 *   modes = {
 *     "test" = "Testing",
 *     "live" = "Live"
 *   },
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_bridgepay\PluginForm\TokenPayPaymentMethodAddForm"
 *   },
 *   payment_method_types = {"credit_card"}
 * )
 *
 * @package Drupal\commerce_bridgepay\Plugin\Commerce\PaymentGateway
 */
class BridgePay extends OnsitePaymentGatewayBase implements RequestDefaultsInterface {

  /**
   * @var \BridgeComm\Client
   */
  protected $bridgepayHttpClientFactory;

  /**
   * @var \BridgeComm\RequestFactory
   */
  protected $bridgepayRequestFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('bridgepay.http_client.factory')
    );
  }

  /**
   * BridgePay constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\commerce_bridgepay\BridgePayHttpClientFactory $bridgepay_http_client_factory
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    BridgePayHttpClientFactory $bridgepay_http_client_factory
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->bridgepayHttpClientFactory = $bridgepay_http_client_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_code' => '',
      'merchant_account_code' => '',
      'user' => '',
      'password' => '',
      'token_pay' => TRUE,
      'public_key' => '',
      'private_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Code'),
      '#description' => $this->t("The merchant code."),
      '#default_value' => $this->configuration['merchant_code'],
      '#required' => TRUE,
    ];

    $form['merchant_account_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Account Code'),
      '#description' => $this->t("The merchant account code."),
      '#default_value' => $this->configuration['merchant_account_code'],
      '#required' => TRUE,
    ];

    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Name'),
      '#description' => $this->t("The user name"),
      '#default_value' => $this->configuration['user'],
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t("The user password"),
      '#default_value' => $this->configuration['password'],
      '#required' => TRUE,
    ];

    $form['token_pay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use TokenPay.js to collect payment card details'),
      '#default_value' => !empty($this->configuration['token_pay']),
    ];

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#description' => $this->t('The private key in use with token pay.'),
      '#default_value' => $this->configuration['private_key'],
    ];

    $form['public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key'),
      '#description' => $this->t('The public key in use with token pay.'),
      '#default_value' => $this->configuration['public_key'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_code'] = $values['merchant_code'];
      $this->configuration['merchant_account_code'] = $values['merchant_account_code'];
      $this->configuration['user'] = $values['user'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['token_pay'] = !empty($values['token_pay']);
      $this->configuration['private_key'] = $values['private_key'];
      $this->configuration['public_key'] = $values['public_key'];
    }
  }

  /**
   * Get the bridgcomm client.
   *
   * @return \BridgeComm\Client
   */
  protected function bridgepayClient(): Client {
    return new Client(
      $this->getMode() === 'test' ?
        $this->bridgepayHttpClientFactory->getTest() :
        $this->bridgepayHttpClientFactory->getLive()
    );
  }

  /**
   * Get the request factory.
   *
   * @return \BridgeComm\RequestFactory
   */
  protected function bridgepayRequestFactory(): RequestFactory {
    if (!isset($this->bridgepayRequestFactory)) {
      $this->bridgepayRequestFactory = new RequestFactory($this);
    }

    return $this->bridgepayRequestFactory;
  }

  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $payment_method->setReusable(TRUE);
    $payment_method->save();
  }

  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \BridgeComm\RequestMessage\ProcessPaymentRequestMessage $message */
    $message = $this->bridgepayRequestFactory()->createRequestMessage(Request::R_PROCESS_PAYMENT);
    $message->setAmount((int) Calculator::multiply($payment->getAmount()->getNumber(), 100));
    $message->setTransactionType(
      $capture ? ProcessPaymentRequestMessage::TT_SALE : ProcessPaymentRequestMessage::TT_SALE_AUTH
    );

    if ($payment_method->bundle() === 'credit_card') {
      if ($token = $payment_method->getRemoteId()) {
        $message->setCardToken($token, "{$payment_method->card_expiry_month}{$payment_method->card_expiry_year}");
      }

      throw new HardDeclineException('Direct Credit Card requests are not supported.');
    }

    try {
      $request = $this->bridgepayRequestFactory()->createRequest($message);
      $response = $this->bridgepayClient()->sendRequest($request);
    }
    catch (ResponseException $exception) {
      throw new PaymentGatewayException($exception->getMessage());
    }

    $payment->setRemoteId($response->getMessage()->getGatewayTransId());
    $payment->setState($capture && $payment_method->bundle() == 'credit_card' ? 'completed' : 'authorization');
    $payment->save();
  }

  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getSoftwareVendor(): string {
    return "Drupal - Commerce BridgePay - 1";
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantCode(): string {
    return $this->configuration['merchant_code'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantAccountCode(): string {
    return $this->configuration['merchant_account_code'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials(): RequestCredentialsInterface {
    return new RequestCredentials($this->configuration['user'], $this->configuration['password']);
  }
}

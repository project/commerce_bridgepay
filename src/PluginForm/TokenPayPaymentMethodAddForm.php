<?php

namespace Drupal\commerce_bridgepay\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

class TokenPayPaymentMethodAddForm extends PaymentMethodAddForm {

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $gateway_plugin = $payment_method->getPaymentGateway()->getPlugin();

    $form['#attached']['library'][] = 'commerce_bridgepay/tokenpayjs.'.$gateway_plugin->getMode();
    $form['#attached']['drupalSettings']['bridgepayTokenPay']['publicKey'] = $gateway_plugin->getConfiguration()['public_key'];
    $form['payment_details'] = [
      '#parents' => array_merge($form['#parents'], ['payment_details']),
      '#type' => 'container',
      '#payment_method_type' => $payment_method->bundle(),
    ];
    $form['payment_details']['token'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['token-target'],
      ],
      '#default_value' => '',
    ];
    $form['payment_details']['card'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['token-card'],
      ],
    ];
    $form['payment_details']['error_message'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['token-error-message'],
      ],
    ];

    return $form;
  }

}

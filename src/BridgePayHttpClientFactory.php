<?php

namespace Drupal\commerce_bridgepay;

use GuzzleHttp\Client;

class BridgePayHttpClientFactory {

  /**
   * Constants for the right base uri.
   */
  const TEST_BASE_URI = 'https://www.bridgepaynetsecuretest.com/PaymentService/';
  const LIVE_BASE_URI = 'https://bridgepaynetsecuretx.com/PaymentService/';

  /**
   * Return a configured http client.
   */
  public function getTest() {
    return new Client([
      'base_uri' => static::TEST_BASE_URI,
    ]);
  }

  public function getLive() {
    return new Client([
      'base_uri' => static::LIVE_BASE_URI,
    ]);
  }
}
